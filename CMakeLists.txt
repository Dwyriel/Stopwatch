cmake_minimum_required(VERSION 3.25)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_INCLUDE_CURRENT_DIR ON)

add_subdirectory(StopwatchStatic)
add_subdirectory(StopwatchHeaderOnly)

add_library(Stopwatch ALIAS StopwatchStatic)
