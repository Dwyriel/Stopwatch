#ifndef STOPWATCH_STOPWATCH_H
#define STOPWATCH_STOPWATCH_H

#include <chrono>

/**
 * <br>Uses std::chrono::high_resolution_clock
 */
class StopwatchHighRes {
    std::chrono::time_point<std::chrono::high_resolution_clock> m_start, m_end;
    double overheadInPico = 0;
    bool isRunning = false;

    template<class Ratio>
    double elapsedByPrecision();

public:
    StopwatchHighRes();

    void start();

    void stop();

    /**
     * <br>Should not be called after calling start.
     */
    void calculateOverhead();

    void resetOverhead();

    /**
     * @return overhead in picoseconds.
     */
    double overhead();

    double elapsedPicoseconds();

    double elapsedNanoseconds();

    double elapsedMicroseconds();

    double elapsedMilliseconds();

    double elapsedSeconds();
};

/**
 * <br>Uses std::chrono::steady_clock
 */
class StopwatchSteady {
    std::chrono::time_point<std::chrono::steady_clock> m_start, m_end;
    double overheadInPico = 0;
    bool isRunning = false;

    template<class Ratio>
    inline double elapsedByPrecision();

public:
    StopwatchSteady();

    void start();

    void stop();

    /**
     * <br>Should not be called after calling start.
     */
    void calculateOverhead();

    void resetOverhead();

    /**
     * @return overhead in picoseconds.
     */
    double overhead();

    double elapsedPicoseconds();

    double elapsedNanoseconds();

    double elapsedMicroseconds();

    double elapsedMilliseconds();

    double elapsedSeconds();
};

/**
 * <br>Default Stopwatch, uses std::chrono::high_resolution_clock
 */
using Stopwatch = StopwatchHighRes;

#endif //STOPWATCH_STOPWATCH_H
