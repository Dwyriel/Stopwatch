cmake_minimum_required(VERSION 3.25)

project(StopwatchStatic LANGUAGES CXX VERSION 1.1)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_INCLUDE_CURRENT_DIR ON)

add_library(StopwatchStatic STATIC stopwatch.cpp)
