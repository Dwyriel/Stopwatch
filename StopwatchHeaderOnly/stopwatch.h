#ifndef STOPWATCH_STOPWATCH_H
#define STOPWATCH_STOPWATCH_H

#include <chrono>

#define TOTAL_OVERHEAD_PASSES 10

template<class clock>
class Stopwatch_t {
    std::chrono::time_point<clock> m_start, m_end;
    double overheadInPico = 0;
    bool isRunning = false;

public:
    Stopwatch_t() : m_start(clock::now()), m_end(clock::now()) {
        calculateOverhead();
    }

    void start() {
        isRunning = true;
        m_start = clock::now();
    }

    void stop() {
        m_end = clock::now();
        isRunning = false;
    }

    /**
     * <br>Should not be called after calling start() and before calling stop().
     */
    void calculateOverhead() {
        overheadInPico = 0;
        double results[TOTAL_OVERHEAD_PASSES];
        for (double &result: results) {
            start();
            stop();
            result = elapsedPicoseconds();
        }
        double total = 0;
        for (const double result: results)
            total += result;
        overheadInPico = total / TOTAL_OVERHEAD_PASSES;
    }

    void resetOverhead() {
        overheadInPico = 0;
    }

    /**
     * @return overhead in picoseconds.
     */
    [[nodiscard]] double overhead() const {
        return overheadInPico;
    }

    template<typename Ratio>
    double elapsedByPrecision() {
        if (isRunning)
            m_end = clock::now();
        std::chrono::duration<double, Ratio> elapsed = m_end - m_start;
        double convertedOverhead = overheadInPico * (static_cast<double>(Ratio::den) / static_cast<double>(std::pico::den));
        return elapsed.count() - convertedOverhead;
    }

    double elapsedPicoseconds() {
        return elapsedByPrecision<std::pico>();
    }

    double elapsedNanoseconds() {
        return elapsedByPrecision<std::nano>();
    }

    double elapsedMicroseconds() {
        return elapsedByPrecision<std::micro>();
    }

    double elapsedMilliseconds() {
        return elapsedByPrecision<std::milli>();
    }

    double elapsedSeconds() {
        return elapsedByPrecision<std::ratio<1> >();
    }
};

using Stopwatch = Stopwatch_t<std::chrono::high_resolution_clock>;
using StopwatchSystem = Stopwatch_t<std::chrono::system_clock>;
using StopwatchHighRes = Stopwatch_t<std::chrono::high_resolution_clock>;
using StopwatchSteady = Stopwatch_t<std::chrono::steady_clock>;

#undef TOTAL_OVERHEAD_PASSES

#endif //STOPWATCH_STOPWATCH_H
